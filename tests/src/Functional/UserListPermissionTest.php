<?php

declare(strict_types=1);

namespace Drupal\Tests\user_list_permission\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests user list permission.
 *
 * @group user_list_permission
 */
final class UserListPermissionTest extends BrowserTestBase {

  protected static $modules = [
    'user_list_permission',
    'views',
    'user',
    'system',
  ];

  protected $defaultTheme = 'stark';

  /**
   * Tests user with user.module permission cannot access people list.
   */
  public function testAdministerPermission(): void {
    $admin = $this->drupalCreateUser(['administer users']);
    $this->drupalLogin($admin);
    $this->drupalGet(Url::fromRoute('entity.user.collection'));
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests user with our list permission can access people list.
   */
  public function testListPermission(): void {
    $admin = $this->drupalCreateUser(['access people list']);
    $this->drupalLogin($admin);
    $this->drupalGet(Url::fromRoute('entity.user.collection'));
    $this->assertSession()->statusCodeEquals(200);
  }

}
