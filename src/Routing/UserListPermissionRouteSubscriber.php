<?php

declare(strict_types=1);

namespace Drupal\user_list_permission\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Alters permission of user collection route.
 */
final class UserListPermissionRouteSubscriber extends RouteSubscriberBase {

  protected function alterRoutes(RouteCollection $collection): void {
    // Modifies the non-View route provided user.module.
    if ($route = $collection->get('entity.user.collection')) {
      // Only modify if it is unchanged from core.
      if ($route->getRequirement('_permission') === 'administer account settings') {
        $route->setRequirement('_permission', 'access people list');
      }
    }
  }

}
